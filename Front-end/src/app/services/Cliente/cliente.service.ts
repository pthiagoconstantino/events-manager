import { Cliente } from "./../../model/cliente/cliente";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment as env } from "../../../environments/environment";
import { Response, ResponseType } from "@angular/http";
@Injectable({
  providedIn: "root",
})
export class ClienteService {
  private readonly PATH_INSERT_CLIENTE = "/cliente/insert";
  private readonly PATH_LISTA_CLIENTE = "/cliente/list";
  private readonly PATH_UPDATE_CLIENTE = "/cliente/update/";
  private readonly PATH_SEARCH_BY_CPF= "/cliente/findbycpf/";

  constructor(private http: HttpClient) {}

  listarClientes(): Observable<any> {
    return this.http.get(env.baseApiUrl + this.PATH_LISTA_CLIENTE);
  }

  cadastrarCliente(cliente: Cliente): Observable<any> {
    return this.http.post(env.baseApiUrl + this.PATH_INSERT_CLIENTE, cliente, {responseType:'text'});
  }

  alterarCliente(cpf:string, cliente: Cliente): Observable<any> {
    return this.http.put(env.baseApiUrl + this.PATH_UPDATE_CLIENTE+cpf, cliente, {responseType:'text'} );
  }

  buscarPorCpf(cpf: string): Observable<any> {
    return this.http.get(env.baseApiUrl + this.PATH_SEARCH_BY_CPF+cpf);
  }
}

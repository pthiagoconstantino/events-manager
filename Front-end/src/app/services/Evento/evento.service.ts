import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Evento } from "./../../model/evento/evento";
import { environment  as env} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  private readonly PATH_LISTA_EVENTO = '/evento/list';
  private readonly PATH_INSERT_CLIENTE = "/evento/insert";
  private readonly PATH_DELET_EVENTO = "/evento/delet/";
  private readonly PATH_UPDATE_EVENTO = "/evento/update/";
  private readonly PATH_SEARCH_BY_DATE_EVENTO = "/evento/findbydate/";

  constructor(
    private http:HttpClient){

    }

  listarEventos():Observable<any> {
    return this.http.get(env.baseApiUrl + this.PATH_LISTA_EVENTO);
  }

  cadastrarEvento(evento: Evento): Observable<any> {
    return this.http.post(env.baseApiUrl + this.PATH_INSERT_CLIENTE, evento, {responseType:'text'});
  }
  
  removerEvento(data: Date): Observable<any> {
    return this.http.delete(env.baseApiUrl + this.PATH_DELET_EVENTO+data, {responseType:'text'});
  }

  alterarEvento(data:string, evento:Evento): Observable<any> {
    return this.http.put(env.baseApiUrl + this.PATH_UPDATE_EVENTO+data, evento, {responseType:'text'} );
  }

  buscarPorData(data: string): Observable<any> {
    return this.http.get(env.baseApiUrl + this.PATH_SEARCH_BY_DATE_EVENTO+data);
  }

}

import { EventoService } from "./../services/Evento/evento.service";
import { Component, OnInit } from "@angular/core";
import { Cliente } from "../model/cliente/cliente";
import { ClienteService } from "../services/Cliente/cliente.service";
import { Evento } from "../model/evento/evento";
import { ToastrService } from "ngx-toastr";
import { SidebarComponent } from "../components/sidebar/sidebar.component";
import { Router, RouterLink } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AtualizarEventoComponent } from "../atualizar-evento/atualizar-evento.component";
import { AtualizarClienteComponent } from "../atualizar-cliente/atualizar-cliente.component";


@Component({
  selector: "app-table-list",
  templateUrl: "./table-list.component.html",
  styleUrls: ["./table-list.component.css"],
})
export class TableListComponent implements OnInit {
  clientes: Cliente[];

  eventos: Evento[];

  coluns: string[];

  constructor(
    private modalService: NgbModal,
    private clienteService: ClienteService,
    private eventoService: EventoService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.listarClientes();
    this.listarEventos();
  }
  listarClientes() {
    this.clienteService.listarClientes().subscribe((data) => {
      this.clientes = data;
    });
  }

  listarEventos() {
    this.eventoService.listarEventos().subscribe((data) => {
      this.eventos = data;
    });
  }

  abreAlterarEvento(eventodate:any){
    const modaRef = this.modalService.open(AtualizarEventoComponent,{
  size:'lg',
  backdrop: 'static',
})
modaRef.componentInstance.eventodate = eventodate;
 }

 abreAlterarCliente(cpfParametro:any){
  const modaRef = this.modalService.open(AtualizarClienteComponent,{
size:'lg',
backdrop: 'static',
})
modaRef.componentInstance.cpfParametro = cpfParametro;
}

  removerEvento(dataEvento: Date) {
        
           this.eventoService.removerEvento(dataEvento).subscribe(
      data => {
    
        const msg:string ='Evento removido com Sucesso!';
this.toastr.success(msg,'SUCESSO');
setTimeout(() => {
  window.location.replace("http://localhost:4200/lista");
}, 1000);
},
err => {
let msg: string = 'Evento não existe no banco!.';
if(err.status == 401){
  return msg = err.error.errors.join('  ');
}
console.log(err);
this.toastr.error(msg,'ERRO');
    })
  
  }

  redirecionar(){
    RouterLink
  }
}


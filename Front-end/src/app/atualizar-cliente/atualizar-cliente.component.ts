import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from "ngx-toastr";
import { Cliente } from "../model/cliente/cliente";
import { ClienteService } from "../services/Cliente/cliente.service";

@Component({
  selector: 'app-atualizar-cliente',
  templateUrl: './atualizar-cliente.component.html',
  styleUrls: ['./atualizar-cliente.component.css']
})
export class AtualizarClienteComponent implements OnInit {
 
  @Input() public cpfParametro: string;

  cliente:Cliente;

  generos: string[];
  
  form!: FormGroup;

  clientes: Cliente[];

  constructor(

    private clienteService: ClienteService,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    public modalService: NgbModal,
    private toastr: ToastrService,

    
  ) {
    this.cliente = new Cliente();   
  }

  ngOnInit() { 
    this.cliente = new Cliente();
    this. clienteByCpf();
    this.gerarForm(this.cliente);
    this.listaGeneros();
    console.log(this.cliente.dataNascimento);
    }

  tipo:string;

  dataEvento:string;
  
  dataCadastro:string;
  
  nomeCliente:string;

  
 clienteByCpf(){
    this.clienteService.buscarPorCpf(this.cpfParametro)
    .subscribe(
      data => {
        this.cliente = data;
        this.gerarForm( this.cliente);
  
      }
    )
  }

  gerarForm(cliente:Cliente) {
    this.form = this.fb.group({
      nome: [cliente.nome, [Validators.required]],
      genero: [cliente.genero, [Validators.required]],
      telefone: [cliente.telefone, [Validators.required]],
       cpf: [cliente.cpf, [Validators.required]],
        dataNascimento: [cliente.dataNascimento2, [Validators.required]],
    });
  }
   atualizarCliente(cpf: string) {
      if (this.form.invalid) {
      this.toastr.warning("Digite os campos corretamente", "AVISO");
      return;
    }
    const cliente: Cliente = this.form.value;
    console.log(cliente);
    this.clienteService.alterarCliente(this.cpfParametro, cliente).subscribe(
      data => {
        const msg: string = 'Cliente Atualizado com sucesso!';
        this.toastr.success(msg,"SUCESSO");
        setTimeout(() => {
          this.activeModal.dismiss(this.toastr.success)
          window.location.replace("http://localhost:4200/lista");
        }, 1500);

      },
      (err) => {        
        
        let msg: string  = "ja existe um cliente com esse cpf cadastrado";

        if (err.status == 403) {
          return (msg = err.error.errors.join("  "));
           }   
        
        this.toastr.error(msg, "Erro!");
      });
 
  }

  listaGeneros(){
    this.generos= ["Maculino", "Femenino", "Outros"]
  }

}

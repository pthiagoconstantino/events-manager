import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { NgbModal, NgbModalModule, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ToastrModule } from "ngx-toastr";

import { AppRoutingModule } from "./app.routing";
import { ComponentsModule } from "./components/components.module";

import { AppComponent } from "./app.component";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { AtualizarClienteComponent } from './atualizar-cliente/atualizar-cliente.component';
import { DatePipe } from "@angular/common";


@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    NgbModalModule,
  ],
  declarations: [
    AppComponent, 
    AdminLayoutComponent
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}

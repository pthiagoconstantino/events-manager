import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { EventoService } from "../services/Evento/evento.service";
import { Evento } from '../model/evento/evento';
import { ClienteService } from '../services/Cliente/cliente.service';
import { Cliente } from '../model/cliente/cliente';
import { DatePipe } from '@angular/common';




@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  form!: FormGroup;
  clientes: Cliente[];
  constructor(
    private datePipe: DatePipe,
    private clienteService: ClienteService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private eventoService: EventoService
  ) {}

  ngOnInit() {
    this.listarClientes();
    this.gerarForm();
  }
  
  gerarForm() {
    this.form = this.fb.group({
      tipo: ['', [Validators.required]],
      dataEvento2: ['', [Validators.required]],
      cliente: ['', [Validators.required]],
      });
  }

  listarClientes() {
    this.clienteService.listarClientes().subscribe((data) => {
      this.clientes = data;
    });
  }
;
  cadastrarEvento() {
        if (this.form.invalid) {
      this.toastr.warning("Digite os campos corretamente", "AVISO");
      return;
    }
    const evento: Evento = this.form.value;
   
      this.eventoService.cadastrarEvento(evento).subscribe(
      (data) => {
        const msg: string = 'Evento reservado com sucesso!';
         this.toastr.success(msg, "SUCESSO");
         setTimeout(() => {
          this.form.reset();
         window.location.replace("http://localhost:4200/lista");
       }, 2000);
      },
      (err) => {
     
        let msg: string  = "ja existe um  Evento reservado para esta data";
        if (err.status == 403) {
          return (msg);
        }
        this.toastr.error(msg, "ERRO");
      }
    );

  }
}

import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { Cliente } from "../model/cliente/cliente";
import { ClienteService } from "../services/Cliente/cliente.service";


@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
})

export class UserProfileComponent implements OnInit {
  form!: FormGroup;
  generos: string[];
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private clienteService: ClienteService
  ) {}

  ngOnInit() {
    this.gerarForm();
    this.listaGeneros();
  }

  gerarForm() {
    this.form = this.fb.group({
      nome: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      telefone: ['', [Validators.required]],
       cpf: ['', [Validators.required]],
        dataNascimento: ['', [Validators.required]],
    });
  }
  cadastrarCliente() {
      if (this.form.invalid) {
      this.toastr.warning("Digite os campos corretamente", "AVISO");
      return;
    }
    const cliente: Cliente = this.form.value;
     this.clienteService.cadastrarCliente(cliente).subscribe(
      data => {
        const msg: string = 'Cliente cadastrado com sucesso!';
        this.toastr.success(msg,"SUCESSO");
        setTimeout(() => {
          this.form.reset();
         window.location.replace("http://localhost:4200/lista");
       }, 2000);

      },
      (err) => {        
        
        let msg: string  = "ja existe um cliente com esse cpf cadastrado";

        if (err.status == 403) {
          return (msg = err.error.errors.join("  "));
           }   
        
        this.toastr.error(msg, "Erro!");
      });
 
  }

  listaGeneros(){
    this.generos= ["Maculino", "Femenino", "Outros"]
  }

}

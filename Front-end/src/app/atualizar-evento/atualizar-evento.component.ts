import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { EventoService } from "../services/Evento/evento.service";
import { Evento } from '../model/evento/evento';
import { ClienteService } from '../services/Cliente/cliente.service';
import { Cliente } from '../model/cliente/cliente';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-atualizar-evento',
  templateUrl: './atualizar-evento.component.html',
  styleUrls: ['./atualizar-evento.component.css']
})
export class AtualizarEventoComponent implements OnInit {

  @Input() public eventodate: string;
  evento:Evento;
  
  form!: FormGroup;

  clientes: Cliente[];

  constructor(

    private clienteService: ClienteService,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    public modalService: NgbModal,
    private toastr: ToastrService,
    private eventoService: EventoService
    
  ) {
    this.evento = new Evento();   
  }

  ngOnInit() { 
    this.evento = new Evento();
    this.listarClientes();
     this. eventoByDate();
    this.gerarForm(this.evento);
    
    }

  gerarForm(evento: Evento) {
    this.form = this.fb.group({
      tipo: [evento.tipo, [Validators.required]],
      dataEvento2: [evento.dataEvento, [Validators.required]],
      cliente: [evento.cpfCliente, [Validators.required]],
      })
    
  }
 eventoByDate(){
    this.eventoService.buscarPorData(this.eventodate)
    .subscribe(
      data => {
        this.evento = data;
        this.gerarForm( this.evento);
  
      }
    )
  }

  listarClientes() {
    this.clienteService.listarClientes().subscribe((data) => {
      this.clientes = data;
    });
  }
;

  atualizarEvento() {
   console.log("",this.form);
         if (this.form.invalid) {
      this.toastr.warning("Digite os campos corretamente", "AVISO");
      return;
    }
    const evento: Evento = this.form.value;

      this.eventoService.alterarEvento(this.eventodate, evento ).subscribe(
      (data) => {
        const msg: string = 'Evento reservado com sucesso!';
         this.toastr.success(msg, "SUCESSO");

         setTimeout(() => {
          this.activeModal.dismiss(this.toastr.success)
          window.location.replace("http://localhost:4200/lista");
        }, 1500);

           

      },
      (err) => {
     
        let msg: string  = "ja existe um  Evento reservado para esta data";
        if (err.status == 403) {
          return (msg);
        }
        this.toastr.error(msg, "ERRO");
      }
    );

  }
}

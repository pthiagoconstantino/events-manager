package com.api.events.dtos;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class EventoDto {
	
	private String tipo;

	@Temporal(TemporalType.DATE)
	private Date dataEvento;
	
	private String dataCadastro;

	private String Cliente;
	
	private String cpfCliente;
	
	private String dataEvento2;
	
}

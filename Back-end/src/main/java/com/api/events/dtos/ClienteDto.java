package com.api.events.dtos;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class ClienteDto {
	
	private String nome;

	private String genero;	
	
	private String telefone;
	
	private String cpf;
	
	private String dataNascimento;
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimento2;
			
}

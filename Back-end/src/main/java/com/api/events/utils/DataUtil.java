package com.api.events.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataUtil {

	public String dateToString(Date data) {

		log.info("Convertendo Data em DataString");

		SimpleDateFormat DateFor = new SimpleDateFormat("dd-MM-yyyy");
		String stringDate = DateFor.format(data);
		System.out.println("Date Format with dd-MM-yyyy" + stringDate);

		return stringDate;

	}

	public Date stringToDate(String dataRecebida) throws ParseException {

		log.info("Convertendo DataString em Data");

		SimpleDateFormat formato = null;
		try {
			formato = new SimpleDateFormat("yyyy-MM-dd");
		} catch (Exception e) {

			e.getMessage();
		}
		Date dataFormatada = formato.parse(dataRecebida);
		
		return dataFormatada;
	}
	

	public Date stringToDate2(String dataRecebida) throws ParseException {

		log.info("Convertendo DataString em Data");
		
		

		SimpleDateFormat formato = null;
		try {
			formato = new SimpleDateFormat("dd-MM-yyyy");
		} catch (Exception e) {

			e.getMessage();
		}
		Date dataFormatada = formato.parse(dataRecebida);
		log.info("dataformatada, {}", dataFormatada);

		return dataFormatada;
	}

}

package com.api.events.controllers;

import java.net.URI;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.api.events.dtos.ClienteDto;
import com.api.events.services.ClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping(value = "/api/cliente")
@Api(value = "Cliente")
public class ClienteController {
	
	@Autowired
	ClienteService clienteService;

	@PostMapping("/insert")
	@ApiOperation(value = "Adiciona o nova Cliente")
	public ResponseEntity<?> insert(@RequestBody ClienteDto clienteDto) throws ParseException, Throwable {
	
		if (clienteDto == null) {

			log.error("Cliente chegou como nulo");
	
			return ResponseEntity.noContent().build();

		}
		
		else {
			
			if((clienteService.findbycpf(clienteDto.getCpf()))==true){
				log.error("ja existe um cliente com esse cpf cadastrado" );
				return new ResponseEntity<String>("ja existe um cliente com esse cpf cadastrado, agende em outro cliente" ,HttpStatus.UNAUTHORIZED);
			}

		log.info("Cliente chego com dados {}", clienteDto.toString());

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(clienteService.insert(clienteDto).getId()).toUri();

		return ResponseEntity.created(uri).build();

		}

	}
		
	@GetMapping("/lista")
	@ApiOperation(value = "Retorna lista de clientes")
	ResponseEntity<?> findAll() {
		
		Optional<List<ClienteDto>> clientes = clienteService.findall();
		
		if (clientes == null) {
			log.error("lista de clientes é nula");
			return ResponseEntity.notFound().build();

		} else {
			log.info("retornando lista da clientes");

			return ResponseEntity.status(HttpStatus.OK).body(clientes);
		}

	}
	
	@DeleteMapping("/delet/{cpf}")
	public ResponseEntity<?> delet(@PathVariable String cpf) throws ParseException{  

		if (cpf == null) {
			log.error("Cliente chegou como nulo");
			return ResponseEntity.noContent().build();
		}

		if((clienteService.findbycpf(cpf))==true){
			log.info("existe um cliente com esse cpf cadastrado" );
			clienteService.delet(cpf);
			return ResponseEntity.ok().body("Cliente removido com sucesso!");
		}
		
		else {
			log.error("cliente não existe no banco");
			return ResponseEntity.notFound().build();
		}
		
	}
	
	
//	@GetMapping("/atualizar")
//	@ApiOperation(value = "Atualizar cliente")
//	ResponseEntity<?> update(@RequestBody ClienteDto clienteDto) throws ParseException, Throwable {
//		
//				
//		if (clienteDto == null) {
//			log.error("Cliente chegou como nulo");	
//			return ResponseEntity.noContent().build();
//		}
//		
//		else {
//			
//			if((clienteService.findbycpf(clienteDto.getCpf()))==true){
//				log.error("ja existe um cliente com esse cpf cadastrado" );
//				return new ResponseEntity<String>("ja existe um cliente com esse cpf cadastrado, agende em outro cliente" ,HttpStatus.UNAUTHORIZED);
//			}
//
//		log.info("Cliente chego com dados {}", clienteDto.toString());
//
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//				.buildAndExpand(clienteService.insert(clienteDto).getId()).toUri();
//
//		return ResponseEntity.created(uri).build();
//
//		}
//	}
}

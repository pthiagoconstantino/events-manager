package com.api.events.controllers;

import java.net.URI;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.api.events.dtos.EventoDto;
import com.api.events.services.EventoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/api/evento")
@Api(value = "evento")
@Slf4j
public class EventoController {
	
	
	
	@Autowired
	EventoService eventoService;
	
	@PostMapping("insert")
	@ApiOperation(value = "Adiciona o novo Evento")
	public ResponseEntity<?> insert(@RequestBody EventoDto eventoDto) throws ParseException, Throwable {
	
		if (eventoDto == null) {
			log.error("Evento chegou como nulo");
			return ResponseEntity.noContent().build();
		}
		if(eventoService.Verificarhoraevento(eventoDto.getDataEvento2())==true){
			log.error("ja existe um Horario marcado nesse dia" );
			return new ResponseEntity<String>("ja existe um Horario marcado nesse dia, agende em outro dia" ,HttpStatus.UNAUTHORIZED);
		}
		
		else {			
		log.info("Evento chegou com dados {}", eventoDto.toString());

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(eventoService.insert(eventoDto).getId()).toUri();
		return ResponseEntity.created(uri).body("Evento reservado com Sucesso!");

		}
	}
	
	@GetMapping("/list")
	@ApiOperation(value = "Retorna lista de eventos")
	public ResponseEntity<?> findAll() {
		
		Optional<List<EventoDto>> eventos = eventoService.findall();
		
		if (eventos.isPresent()) {
			log.info("retornando lista da eventos");
			return ResponseEntity.status(HttpStatus.OK).body(eventos);
		} else {			
			log.error("lista de eventos é nula");
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("/delet/{data}")
	public ResponseEntity<?> delet(@PathVariable String data) throws ParseException  {
		
		if (data == null) {
			log.error("Evento chegou como nulo");
			return ResponseEntity.noContent().build();
		}
		
		if(eventoService.Verificarhoraevento(data) == true) {
			log.info("Evento existe no banco");
			eventoService.delet(data);
			return ResponseEntity.ok().body("Evento removido com sucesso!");
		}
		
		else {
			log.error("Evento não existe no banco");
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("/update/{date}")
	@ApiOperation(value = "atualizar Evento")
	ResponseEntity<?> update(@PathVariable String date, @RequestBody EventoDto eventoDto)
			throws ParseException {
		
		if (eventoDto == null || date == null) {
			log.error("Evento ou date de pesquisa chegou como nulo");
			return ResponseEntity.noContent().build();
		}
		
		if(eventoService.Verificarhoraevento(eventoDto.getDataEvento2())==true && eventoDto.getDataEvento2().equals(date)==false){
			log.error("ja existe um Horario marcado nesse dia" );
			return new ResponseEntity<String>("ja existe um Horario marcado nesse dia, agende em outro dia" ,HttpStatus.UNAUTHORIZED);
		}

		else {
			eventoService.update(date,eventoDto);
			log.error("Evento atualizado");
			return ResponseEntity.ok(eventoDto);
		}
	}	
	
	@GetMapping("/findbydate/{date}")
	@ApiOperation(value = "Retorna lista de eventos")
	public ResponseEntity<?> findByDate(@PathVariable String date) throws ParseException {
		
		Optional<EventoDto> eventoDto = eventoService.findByDate(date);
		
		if (eventoDto.isPresent()) {
			log.info("retornando  evento");
			return ResponseEntity.status(HttpStatus.OK).body(eventoDto);
		} else {			
			log.error("evento não encontrado");
			return ResponseEntity.notFound().build();
		}
		
	}
	
}

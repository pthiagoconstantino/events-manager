package com.api.events.models.dao.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.events.models.entities.Evento;

@Repository
public interface EventoRepository extends JpaRepository<Evento, Long> {

	Evento findByDataEvento(Date dataevento);

}

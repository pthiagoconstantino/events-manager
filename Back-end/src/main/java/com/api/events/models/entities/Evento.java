package com.api.events.models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name = "evento")
public class Evento implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String tipo;

	@Temporal(TemporalType.DATE)
	@Column(name = "dataEvento", nullable=false)
	private Date dataEvento;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dataCadastro;

	@OneToOne(cascade = CascadeType.ALL) 
	@JoinColumn(name = "cliente_id", referencedColumnName = "id")
	private Cliente cliente;
	
}

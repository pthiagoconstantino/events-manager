package com.api.events.models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String nome;

	@Column(nullable=true)
	private String genero;	
	
	@Column(nullable=true)
	private String telefone;
	
	@Column(nullable=true)
	private String cpf;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dataCadastro;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=true)
	private Date dataNascimento;
	

}

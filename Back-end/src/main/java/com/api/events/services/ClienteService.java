package com.api.events.services;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import com.api.events.dtos.ClienteDto;
import com.api.events.models.entities.Cliente;

public interface ClienteService {
	
	public Cliente insert(ClienteDto clienteDto) throws FileNotFoundException, ParseException;
	
	public Cliente dtoToEntity(ClienteDto clienteDto) throws ParseException;
	
	public ClienteDto entityToDto(Cliente cliente) throws ParseException;
	
	public Cliente update(ClienteDto clienteDto) throws FileNotFoundException, ParseException;
	
	public Cliente findbynome(String nome);

	public Optional<List<ClienteDto>>findall();
	
	public boolean findbycpf(String cpf);
	
	public void delet(String cpf);
	
	public Cliente cfindbycpf(String cpf);
	
	public Optional<ClienteDto> cfindbycpfo(String cpf) throws ParseException;
	
	
	public Cliente update(String cpf, ClienteDto clienteDto) throws ParseException;
	
}

package com.api.events.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.events.dtos.ClienteDto;
import com.api.events.models.dao.repositories.ClienteRepository;
import com.api.events.models.entities.Cliente;
import com.api.events.services.ClienteService;
import com.api.events.utils.DataUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClienteServiceImpl implements ClienteService{
	
	DataUtil dataUtil = new DataUtil();
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Override
	public Cliente insert(ClienteDto clienteDto) throws ParseException  {
		
		Cliente cliente = dtoToEntity(clienteDto);
		
		log.info("Persistindo Cliente: {}", cliente.toString());

				
		return clienteRepository.save(cliente);
		
	}
	
	@Override
	public Cliente dtoToEntity(ClienteDto clienteDto) throws ParseException{
		
		log.info("Convertendo ClienteDto Para ClienteEntity: {}", clienteDto.toString());
		
		Cliente cliente = new Cliente();
		
		cliente.setNome(clienteDto.getNome());
		cliente.setGenero(clienteDto.getGenero());
		cliente.setTelefone(clienteDto.getTelefone());
		cliente.setCpf(clienteDto.getCpf());
		
		Date data = new Date();
		cliente.setDataCadastro(data);
		cliente.setDataNascimento(dataUtil.stringToDate(clienteDto.getDataNascimento()));
		
	
		return cliente;
		
	}
	
	@Override
	public ClienteDto entityToDto(Cliente cliente) throws ParseException{
		
		log.info("Convertendo ClienteDto Para ClienteEntity: {}", cliente.toString());
		
		ClienteDto clienteDto = new ClienteDto();
		
		clienteDto.setNome(cliente.getNome());
		clienteDto.setGenero(cliente.getGenero());
		clienteDto.setTelefone(cliente.getTelefone());
		clienteDto.setCpf(cliente.getCpf());
		clienteDto.setDataNascimento2(cliente.getDataNascimento());
		
		clienteDto.setDataNascimento(dataUtil.dateToString(cliente.getDataNascimento()));
			
		return clienteDto;
		
	}
	
	@Override
	public Optional<List<ClienteDto>>findall(){
		
		List<ClienteDto> clientesDto = new ArrayList<ClienteDto>();	

		log.info("convertendo lista de cliente obj para lista de clientes dto {}");

		clienteRepository.findAll().forEach(x -> {
         
			try {
				clientesDto.add(this.entityToDto(x));
			} catch (ParseException e) {
				log.error("Erro na conversão de Date para String");
			}
        		 });

		return Optional.ofNullable(clientesDto);

	}
	
	@Override
	public Cliente update(ClienteDto clienteDto) throws ParseException  {
		
		Cliente cliente = dtoToEntity(clienteDto);
		
		log.info("Persistindo Cliente: {}", cliente.toString());

		Date data = new Date();
		
		cliente.setDataCadastro(data);
		
		return clienteRepository.save(cliente);
		
	}
	
	@Override
	public Cliente findbynome(String nome) {

		log.info("Buscando o Cliente pelo nome");

		Cliente cliente = clienteRepository.findByNome(nome);

		return cliente;
		
	}
	
	@Override
	public boolean findbycpf(String cpf) {

		boolean existeCadastro=true;
		
		log.info("Buscando o Cliente pelo cpf");

		Cliente cliente = clienteRepository.findByCpf(cpf);
		
		if(cliente==null) {
			existeCadastro = false;
		}
		
		return existeCadastro;
		
	}
	
	@Override
	public Cliente cfindbycpf(String cpf) {

	return clienteRepository.findByCpf(cpf);
	
	}
	
	@Override
	public Optional<ClienteDto> cfindbycpfo(String cpf) throws ParseException {
		
		Cliente cliente = clienteRepository.findByCpf(cpf);
		
		ClienteDto clienteDto = this.entityToDto(cliente);

	return Optional.ofNullable(clienteDto);
	
	}
	
	
	@Override
	public void delet(String cpf) {
		
		Cliente cliente = clienteRepository.findByCpf(cpf);
		
			clienteRepository.delete(cliente);
	}		
		
	@Override
	public Cliente update(String cpf, ClienteDto clienteDto) throws ParseException {
		
		Cliente clientefind = clienteRepository.findByCpf(cpf);
		
		Cliente clienteNovo = this.dtoToEntity(clienteDto);
		
		clienteNovo.setId(clientefind.getId());
		
		return clienteRepository.save(clienteNovo);
		
	}
}


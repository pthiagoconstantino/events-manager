package com.api.events.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.events.dtos.EventoDto;
import com.api.events.models.dao.repositories.EventoRepository;
import com.api.events.models.entities.Evento;
import com.api.events.services.ClienteService;
import com.api.events.services.EventoService;
import com.api.events.utils.DataUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EventoServiceImpl implements EventoService{
	
	DataUtil dataUtil = new DataUtil();
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	EventoRepository eventoRepository;
	
	@Override
	public Evento insert(EventoDto eventoDto) throws ParseException  {
			
		Evento evento = dtoToEntity(eventoDto);
		
		log.info("Persistindo Evento: {}", evento.toString());

		Date data = new Date();
		
		evento.setDataCadastro(data);
		
		return eventoRepository.save(evento);
				
	}
	
	@Override
	public Evento dtoToEntity(EventoDto eventoDto) throws ParseException{
		
		log.info("Convertendo EventoDto Para EventoEntity: {}", eventoDto.toString());
		
		Evento evento = new Evento();
		
		evento.setTipo(eventoDto.getTipo());
		if(eventoDto.getDataEvento2()== null) {
		evento.setDataEvento(eventoDto.getDataEvento());
		}
		else {
			evento.setDataEvento(dataUtil.stringToDate(eventoDto.getDataEvento2()));
		}
		evento.setCliente(clienteService.cfindbycpf(eventoDto.getCliente()));
			
		return evento;
		
	}
	
	@Override
	public EventoDto entityToDto(Evento evento) throws ParseException{
		
		log.info("Convertendo EventoDto Para EventoEntity: {}", evento.toString());
		
		EventoDto eventoDto = new EventoDto();
		
		eventoDto.setTipo(evento.getTipo());
		eventoDto.setDataEvento(evento.getDataEvento());
		eventoDto.setDataCadastro(dataUtil.dateToString(evento.getDataCadastro()));
		eventoDto.setCliente(evento.getCliente().getNome());
		
		return eventoDto;
		
	}
	
	@Override
	public  Optional<List<EventoDto>>findall(){
		
		List<EventoDto> eventosDto = new ArrayList<EventoDto>();	

		log.info("convertendo lista de evento obj para lista de eventos dto {}");

		eventoRepository.findAll().forEach(x -> {
         
			try {
				eventosDto.add(this.entityToDto(x));
			} catch (ParseException e) {
				log.error("Erro na conversão de Date para String");
			}
        		 });

		return   Optional.ofNullable(eventosDto);

	}
	
	@Override
	public Evento update(EventoDto eventoDto) throws ParseException  {
		
		Evento evento = dtoToEntity(eventoDto);
		
		log.info("Persistindo Evento: {}", evento.toString());

		Date data = new Date();
		
		evento.setDataCadastro(data);
		
		return eventoRepository.save(evento);
		
	}
	
	@Override
	public boolean Verificarhoraevento(Date data) throws ParseException {
				
		Calendar calDataEvento = Calendar.getInstance();
		Calendar calDataVerificar = Calendar.getInstance();
		
		calDataVerificar.setTime(data);
		boolean horarioMarcado = false;
		
		for (Evento evento : eventoRepository.findAll()) {
					  
			calDataEvento.setTime(evento.getDataEvento());
			
			if(calDataEvento.compareTo(calDataVerificar)==0) {
				
				horarioMarcado=true;
			}

			
		}
		
		return horarioMarcado;
	}
	
	public boolean Verificarhoraevento(String data) throws ParseException {
		
		Date date = dataUtil.stringToDate(data);
		
		return this.Verificarhoraevento(date) ;
		
	}
	

	@Override
	public void delet(String dataEvento) throws ParseException {
		
		Date date = dataUtil.stringToDate(dataEvento);
		
		Evento evento = eventoRepository.findByDataEvento(date);
		
		evento.setCliente(null);
		
		eventoRepository.delete(evento);
			
	}		
	
	@Override
	public Evento update(String dateEvento ,EventoDto eventoDto) throws ParseException {
		
		Date date = dataUtil.stringToDate(dateEvento);
		
		Evento eventofind = eventoRepository.findByDataEvento(date);
		
		Evento eventoNovo = this.dtoToEntity(eventoDto);
		
		eventoNovo.setId(eventofind.getId());
		eventoNovo.setDataCadastro(eventofind.getDataCadastro());
		
		return eventoRepository.save(eventoNovo);
		
		
		
	}
	
	@Override
	public Optional<EventoDto> findByDate(String dateEvento) throws ParseException {
		
		Date date = dataUtil.stringToDate(dateEvento);
		
		Evento evento = eventoRepository.findByDataEvento(date);
		
		EventoDto eventoNovo = this.entityToDto(evento);
		
		eventoNovo.setCpfCliente(evento.getCliente().getCpf());
				
		return Optional.ofNullable(eventoNovo);
				
	}
	
		
	
}

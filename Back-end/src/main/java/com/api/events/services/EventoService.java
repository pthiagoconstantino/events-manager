package com.api.events.services;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.api.events.dtos.EventoDto;
import com.api.events.models.entities.Evento;

public interface EventoService {

	public Evento insert(EventoDto EventoDto) throws FileNotFoundException, ParseException;
	
	public Evento dtoToEntity(EventoDto EventoDto) throws ParseException;
	
	public EventoDto entityToDto(Evento Evento) throws ParseException;
	
	public  Optional<List<EventoDto>>findall();
	
	public Evento update(EventoDto EventoDto) throws FileNotFoundException, ParseException;
	
	public boolean Verificarhoraevento(Date data) throws ParseException;
	
	public void delet(String dataEvento) throws ParseException;
	
	public boolean Verificarhoraevento(String data) throws ParseException;
	
	public Evento update(String date ,EventoDto eventoDto) throws ParseException;
	
	public Optional<EventoDto> findByDate(String dateEvento) throws ParseException;
	
}
